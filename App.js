import React, {Component} from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import Login from './components/Login'
import HeroList from './components/HeroList'
import HeroDetail from './components/HeroDetail'
import HeroEvents from './components/HeroEvents'
import VideoComponent from './components/VideoComponent'
import Settings from './components/Settings'
import { Image, TouchableOpacity } from 'react-native'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

const Stack = createStackNavigator()
const Stack2 = createStackNavigator()
const BotTab = createMaterialBottomTabNavigator()

function Index ({navigation}) {
   return(
      <TouchableOpacity onPress={() => {navigation.navigate('Login')}}>
         <Image
            source={require('./components/img/marvelIndex.jpg')}
            style={{width:'100%',height:'100%'}}
         />
      </TouchableOpacity>
   )
}

function HeroListStack() {
   return(
      <Stack2.Navigator initialRouteName="HeroList">
         <Stack2.Screen
            name="HeroList"
            component={HeroList}
            options={{
               title: 'Marvel Characters',
               headerStyle: {
                  backgroundColor: '#151515',
               },
               headerTintColor: 'white',
               headerTitleStyle: {
                  fontWeight: 'bold',
                  fontSize: 20,
               },
            }}
         />
         <Stack2.Screen
            name="HeroDetail"
            component={HeroDetail}
            options={({ route }) => ({
               title: route.params.name,
               headerStyle: {
                  backgroundColor: '#151515',
               },
               headerTintColor: 'white',
               headerTitleStyle: {
                  fontWeight: 'bold',
                  fontSize: 20,
               },
            })}
         />
         <Stack2.Screen
            name="HeroEvents"
            component={HeroEvents}
            options={({ route }) => ({
               title: route.params.name,
               headerStyle: {
                  backgroundColor: '#151515',
               },
               headerTintColor: 'white',
               headerTitleStyle: {
                  fontWeight: 'bold',
                  fontSize: 20,
               },
            })}
         />
      </Stack2.Navigator>
   )
}

function BottomTab() {
   return(
      <BotTab.Navigator
         initialRouteName="List"
         barStyle={{ backgroundColor: '#E62429' }}
      >
         <BotTab.Screen
            name="List"
            component={HeroListStack}
            options={{
               tabBarLabel: 'List',
               tabBarIcon: ({color}) => (
                  <MaterialCommunityIcons name="format-list-bulleted" color={color} size={25} />
               ),
            }}
         />
         <BotTab.Screen
            name="Video"
            component={VideoComponent}
            options={{
               tabBarLabel: 'Video',
               tabBarIcon: ({color}) => (
                  <MaterialCommunityIcons name="airplay" color={color} size={25} />
               ),
            }}
         />
         <BotTab.Screen
            name="Settings"
            component={Settings}
            options={{
               tabBarLabel: 'Settings',
               tabBarIcon: ({color}) => (
                  <MaterialCommunityIcons name="cogs" color={color} size={25} />
               ),
            }}
         />
      </BotTab.Navigator>
   )
}

class App extends Component {
   constructor(props){
      super(props)
      this.state = {}
   }

   render(){
      return(
         <NavigationContainer>
            <Stack.Navigator initialRouteName="Index">
               <Stack.Screen
                  name="Index"
                  component={Index}
                  options={{
                     headerShown: false
                  }}
               />
               <Stack.Screen
                  name="Login"
                  component={Login}
                  options={{
                     headerShown: false
                  }}
               />
               <Stack.Screen
                  name="BotTab"
                  component={BottomTab}
                  options={{
                     headerShown: false
                  }}
               />
            </Stack.Navigator>
         </NavigationContainer>
      )
   }
}

export default App
