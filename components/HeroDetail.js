import React, {Component} from 'react'
import {View, Text, Image, TouchableOpacity, ScrollView} from 'react-native'
import CryptoJS from 'crypto-js'
import Style from './Style'

class HeroDetail extends Component {
   constructor(props) {
      super(props)
      this.state = {
         params: this.props.route.params,
         heroData: {},
         error: null,
         heroThumbnail: {},
         events: [],
         comics: [],
         comicData: {},
         comicThumbnail:{},
         series:[],
      }
   }

   async componentDidMount() {
      let url = `https://gateway.marvel.com:443/v1/public/characters/${this.state.params.heroId}`
      let ts = '9'
      let apikey= '2165d5dbe72c300becd25d840f54d7e8'
      let hash = CryptoJS.MD5(ts+'727368db4ce8c34737b883ca24ecf5e0f333df9a'+apikey)

      await fetch(`${url}?ts=${ts}&apikey=${apikey}&hash=${hash}&orderBy=-modified&limit=10`)
         .then(res => res.json())
         .then(
            result => {
               //console.warn('result', result.data.movies)
               this.setState({
                  heroData: result.data.results[0],
                  heroThumbnail: result.data.results[0].thumbnail,
                  events: result.data.results[0].events.items,
                  comics: result.data.results[0].comics.items,
                  series: result.data.results[0].series.items,
               })
            },
            error => {
               this.setState({
                  error: error,
               })
            }
         )

      await fetch(`${url}/comics?ts=${ts}&apikey=${apikey}&hash=${hash}&title=${this.state.heroData.name}&orderBy=onsaleDate`)
            .then(res => res.json())
            .then(
               result => {
                  this.setState({
                     comicData: result.data.results[Math.round(Math.random() * (3 - 0))],
                     comicThumbnail: result.data.results[0].thumbnail,
                  })
               },
               error => {
                  this.setState({
                     error: error,
                  })
               }
            )
   }

   render() {
      return(
         <ScrollView style={Style.detailScrollView}>
            <View style={[Style.detailSubDeco,{height: 5, width: '100%'}]} />
            <Image
               source={{uri: `${this.state.heroThumbnail.path}.${this.state.heroThumbnail.extension}`}}
               style={Style.detailImage}
            />

            <View style={Style.heroDescContainer}>
               <Text style={Style.heroDesc}>
                  {this.state.heroData.description != "" ? this.state.heroData.description : 'Description not found'}
               </Text>
            </View>

            <View style={Style.detailSubContainer}>
               <Text style={Style.detailSubTitle}>
                  One of the First Comics
               </Text>
               <View style={Style.detailSubDeco}/>
            </View>

            <View style={Style.comicContainer}>
               <View style={Style.comicImageContainer}>
                  <Image 
                     source={{uri: `${this.state.comicThumbnail.path}.${this.state.comicThumbnail.extension}`}}
                     style={Style.comicImage}
                  />
               </View>
               <View style={Style.comicDescContainer}>
                  <Text style={Style.comicTitle}>
                     {this.state.comicData.title}
                  </Text>
                  <Text style={Style.comicDesc}>
                     {this.state.comicData.description != null ? this.state.comicData.description : 'Description not found'}
                  </Text>
               </View>
            </View>
            
            <View style={Style.detailSubContainer}>
               <Text style={Style.detailSubTitle}>
                  Others
               </Text>
               <View style={Style.detailSubDeco}/>
            </View>

            <View style={Style.detailOtherContainer}>
               <View style={Style.otherImageContainer}>
                  <View style={Style.otherImageBorder}>
                     <Image 
                        source={require('./img/Dot.gif')}
                        style={Style.otherImage}
                     />
                  </View>
               </View>
               <View style={Style.otherContainer}>
                  <TouchableOpacity 
                     onPress={() => {this.props.navigation.navigate('HeroEvents', {data: this.state.events, name:'Events'})}}
                     style={Style.otherButton}
                  >
                     <Text style={Style.otherButtonText}>
                        Events Appearance
                     </Text>
                  </TouchableOpacity>

                  <TouchableOpacity 
                     onPress={() => {this.props.navigation.navigate('HeroEvents', {data: this.state.comics, name:'Comics'})}}
                     style={Style.otherButton}
                  >
                     <Text style={Style.otherButtonText}>
                        Comics Appearance
                     </Text>
                  </TouchableOpacity>

                  <TouchableOpacity 
                     onPress={() => {this.props.navigation.navigate('HeroEvents', {data: this.state.series, name:'Series'})}}
                     style={Style.otherButton}
                  >
                     <Text style={Style.otherButtonText}>
                        Series Appearance
                     </Text>
                  </TouchableOpacity>
               </View>
            </View>
         </ScrollView>
      )
   }
}

export default HeroDetail