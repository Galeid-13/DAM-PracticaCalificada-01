import React, {Component} from 'react'
import FontAwesomIcon from 'react-native-vector-icons/FontAwesome'
import { Hideo } from 'react-native-textinput-effects'
import { View, Image, TouchableOpacity, Text, Alert } from 'react-native'
import Style from './Style'

class Login extends Component {
   constructor(props) {
      super(props)
      this.state = {
         inputValue: '',
         passValue: '',
      }
   }

   showAlert = (title, message) => {
      Alert.alert(
         title,
         message,
         [
            {
               text: 'OK',
               onPress: () => console.log('OK Pressed')
            },
         ],
         {cancelable: false},
      )
   }

   changeTextInput = text => {
      this.setState({inputValue: text})
   }
  
   changePasswordInput = text => {
      this.setState({passValue: text})
   }

   validateUser = () => {
      let username = this.state.inputValue
      let password = this.state.passValue

      if (username == ""){
         this.showAlert('Null Username', 'Put a username valid, please.')
         return
      }

      //Aquí va una funcion para la busqueda de nombre de usuario y contraseña
      let userVal = (username == "Bayton")
      let passVal = (password == "123456")

      if (userVal && passVal) {
         //Usuario y Contraseña correctas
         this.props.navigation.navigate('BotTab')
      }else if (userVal) {
         //Solo Usuario correcto
         this.showAlert('Login Failed', 'Password is incorrect. Try Again')
      }else {
         //Intente de nuevo
         this.showAlert('Login Failed', `Doesn't exist the username '${username}'. Try again.`)
      }
   }

   render() {
      return(
         <View style={Style.loginContainer}>
            <View style={Style.loginImageContainer}>
               <Image source={require('./img/marvelLogo.png')} style={Style.loginLogo}/>
            </View>
            <View style={Style.loginInputContainer}>
               <Hideo
                  style={Style.loginInput}
                  onChangeText={text => this.changeTextInput(text)}
                  value={this.state.inputValue}
                  placeholder="Username"
                  iconClass={FontAwesomIcon}
                  iconName={'user'}
                  iconColor={'white'}
                  iconBackgroundColor={'#E62429'}
                  inputStyle={{ color: 'black',}}
               />
               <Hideo
                  style={Style.loginInput}
                  onChangeText={text => this.changePasswordInput(text)}
                  value={this.state.passValue}
                  placeholder="Password"
                  secureTextEntry={true}
                  iconClass={FontAwesomIcon}
                  iconName={'unlock-alt'}
                  iconColor={'white'}
                  iconBackgroundColor={'#E62429'}
                  inputStyle={{ color: 'black'}}
               />
            </View>
            <TouchableOpacity 
               style={Style.loginButton}
               onPress={this.validateUser}
            >
               <Text style={Style.loginButtonText}>
                  LOGIN
               </Text>
            </TouchableOpacity>
         </View>
      )
   }
}

export default Login