import React, {Component} from 'react'
import { TouchableOpacity, FlatList, Image, Text, View } from 'react-native';
import Style from './Style'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

const options = [
   {
      id: 1,
      text: 'Account',
      iconName: 'account'
   },
   {
      id: 2,
      text: 'Privacy and Security',
      iconName: 'shield-check'
   },
   {
      id: 3,
      text: 'Connections',
      iconName: 'signal-variant'
   },
   {
      id: 4,
      text: 'Voice and Video',
      iconName: 'video'
   },
   {
      id: 5,
      text: 'Appearance',
      iconName: 'store'
   },
   {
      id: 6,
      text: 'Notifications',
      iconName: 'bell'
   },
]

function Item({title, iconName}) {
   return(
      <TouchableOpacity style={Style.itemSetting}>
         <View style={[Style.itemDescContainer,{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-start',
            marginVertical: 8,
         }]}>
            <MaterialCommunityIcons name={iconName} color={'white'} size={25} />
            <Text style={[Style.eventText,{marginLeft: 15}]}>{title}</Text>
         </View>
      </TouchableOpacity> 
   )
}

class Settings extends Component {
   constructor(props) {
      super(props)
      this.state = {
      }
   }

   FlatListSeparator = () => {
      return(
         <View style={[Style.itemSeparator,{backgroundColor:"#181818", width:'85%'}]} />
      )
   }

   render() {
      return(
         <View style={Style.loginContainer}>
            <View style={{backgroundColor: '#EE171F', paddingVertical: 50, alignItems: 'center'}}>
               <Image source={require('./img/marvelLogo.png')} style={Style.loginLogo}/>
            </View>            
            <View style={Style.detailSubContainer}>
               <Text style={Style.detailSubTitle}>
                  Settings
               </Text>
               <View style={Style.detailSubDeco}/>
            </View>
            <FlatList
               data={options.length > 0 ? options : []}
               keyExtractor={item => item.id}
               ItemSeparatorComponent={this.FlatListSeparator}
               renderItem={({item}) => (
                  <Item 
                     title={item.text}
                     iconName={item.iconName}
                  />
               )}
               style={{marginTop:25}}
            />
         </View>
      )
   }
}

export default Settings