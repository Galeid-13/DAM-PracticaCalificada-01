import React, {Component} from 'react'
import {View, FlatList, Text, Image, TouchableOpacity} from 'react-native'
import CryptoJS from 'crypto-js'
import Style from './Style'

function Item({id, title, image, comics, events, series, navigation}) {
   return(
      <TouchableOpacity onPress={() => {navigation.navigate('HeroDetail', { heroId: id, name: title})}}>
         <View style={Style.itemContainer}>
            <View style={Style.itemImageContainer}>
               <Image 
                  source= {{uri: image}}
                  style={Style.itemImage} 
               />
            </View>
            <View style={Style.itemDescContainer}>
               <Text style={Style.itemTitle}>
                  {title}
               </Text>
               <Text style={Style.itemSub}>
                  Apariciones en:
               </Text>
               <Text style={Style.itemSub}>
                  Comics: {comics} - Eventos: {events} - Series: {series}
               </Text>
            </View>
         </View>
      </TouchableOpacity> 
   )
}

class HeroList extends Component {
   constructor(props) {
      super(props)
      this.state = {
         items: [],
         error: null,
      }
   }

   async componentDidMount() {
      let url = 'https://gateway.marvel.com:443/v1/public/characters'
      let ts = '9'
      let apikey= '2165d5dbe72c300becd25d840f54d7e8'
      let hash = CryptoJS.MD5(ts+'727368db4ce8c34737b883ca24ecf5e0f333df9a'+apikey)

      await fetch(`${url}?ts=${ts}&apikey=${apikey}&hash=${hash}&orderBy=-modified&limit=10`)
         .then(res => res.json())
         .then(
            result => {
               this.setState({
                  items: result.data.results,
               })
            },
            error => {
               this.setState({
                  error: error,
               })
            }
         )
   }

   FlatListSeparator = () => {
      return(
         <View style={Style.itemSeparator} />
      )
   }

   render() {
      return(
         <View style={Style.listContainer}>
            <View style={[Style.itemSeparator,{height: 4}]} />
            <FlatList
               data={this.state.items.length > 0 ? this.state.items : []}
               keyExtractor={item => item.id}
               ItemSeparatorComponent={this.FlatListSeparator}
               renderItem={({item}) => (
                  <Item 
                     id={item.id}
                     title={item.name} 
                     image={item.thumbnail.path + '.' + item.thumbnail.extension}
                     comics={item.comics.available}
                     events={item.events.available}
                     series={item.series.available}
                     navigation={this.props.navigation}
                  />
               )}
            />
         </View>
      )
   }
}

export default HeroList