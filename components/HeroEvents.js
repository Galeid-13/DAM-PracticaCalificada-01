import React, {Component} from 'react'
import {View, FlatList, Text, TouchableOpacity} from 'react-native'
import Style from './Style'

function Item({title}) {
   return(
      <TouchableOpacity>
         <View style={[Style.itemDescContainer, {margin: 10}]}>
            <Text style={Style.eventText}> -> {title}</Text>
         </View>
      </TouchableOpacity> 
   )
}

class HeroEvents extends Component {
   constructor(props) {
      super(props)
      this.state = {
         data: this.props.route.params.data,
         items: [],
         error: null,
      }
   }

   FlatListSeparator = () => {
      return(
         <View style={Style.itemSeparator} />
      )
   }

   render() {
      return(
         <View style={Style.listContainer}>
            <View style={[Style.itemSeparator,{height: 5}]} />
            <FlatList
               data={this.state.data.length > 0 ? this.state.data : []}
               keyExtractor={item => item.resourceURI}
               ItemSeparatorComponent={this.FlatListSeparator}
               renderItem={({item}) => (
                  <Item 
                     title={item.name} 
                  />
               )}
            />
         </View>
      )
   }
}

export default HeroEvents